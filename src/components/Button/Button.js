import React from 'react';
import './Button.css';

    function printCords(arg) {
        console.log(arg.coords);
    }

    function usersLocation() {
        if (navigator.geolocation) {
                console.log('User Allowed geolocation');
                navigator.geolocation.getCurrentPosition(printCords);
            } else {
                // Add error/denial handling here
            }
    }
    var Button = React.createClass({
      render() {
        return (
            <button className="button" onClick={usersLocation}>Find Great Pizza Around You</button>
        )
      }
    })
export default Button;