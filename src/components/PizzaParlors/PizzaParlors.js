import React from 'react';
import PizzaListItem from '../PizzaListItem/PizzaListItem';
import './PizzaParlors.css';


var PizzaParlors = React.createClass({
    componentWillMount: function() {
        // console.log('componentDidMount()');
    },
    render: function() {
        return (
          <div className="container">
            <h2>Explore great pizza around the world.</h2>
            <PizzaListItem source="https://shipt-pizza-api.herokuapp.com/api/v1/pizzerias" />
          </div>
        )
      }
    })
export default PizzaParlors;