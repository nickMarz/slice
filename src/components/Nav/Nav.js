import React from 'react';
import {NavLink} from 'react-router-dom';

import './Nav.css';

class Nav extends React.Component {
      render() {
        return (
            <div className="nav-links">
            {
            this.props.routes.map(function(comp, index) {
                return (
                    <NavLink key={index} exact={true} activeClassName="active" to={comp.path}>{comp.name}</NavLink>
                    )
                })
            }
            </div>
        )
      }
    }
export default Nav;