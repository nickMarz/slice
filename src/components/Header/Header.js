import React from 'react';
import logo from '../../pizza-icon.svg';

import './Header.css';
class Header extends React.Component {
      render() {
        return (
            <div className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <h1>Find a slice.</h1>
            </div>
        )
      }
    }
export default Header;