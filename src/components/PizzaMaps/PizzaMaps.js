import React from 'react';
import './PizzaMaps.css';
import axios from 'axios';
// import google from 'google-maps-react';

    var PizzaMaps = React.createClass({
    getInitialState: function() {
      return {
        userLat: null,
        userLong: null, 
        serverRequest: null
      }
    },
    componentDidMount: function() {

    },
    componentWillReceiveProps: function(props){
      console.log('\n------------ componentWillReceiveProps -----------')
      var that = this;
      
      that.setState({
              userLat: props.userLat,
              userLong: props.userLong
            })
      this.serverRequest = axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + props.userLat + ',' + props.userLong+ "&key=AIzaSyD4ARHHZgeGcBWpQR0J0D3roXwbd__-_RI")
          .then(function(result) {
            var usersCity = result.data.results[0].address_components[2].long_name;
          })
      },
      componentDidUpdate: function(){
        var google = window.google || {};
        var MarkerClusterer = window.MarkerClusterer || {};

          var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: {lat: this.state.userLat, lng: this.state.userLong}
        })
        var marker = new google.maps.Marker({
          position: {lat: this.state.userLat, lng: this.state.userLong},
          map: map
        });
        console.log(marker, MarkerClusterer)
        // var markerCluster = new MarkerClusterer(map,[],{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
      },
      render() {
        return (
          <div>
            <ul>
              <li>Props</li>
                <li>{this.props.name}</li>
                <li>{this.props.userLat}</li>
                <li>{this.props.userLong}</li>
            </ul>
            <ul>
              <li>State</li>
                <li>{this.state.userLat}</li>
                <li>{this.state.userLong}</li>
            </ul>
              <div id="map"></div>
          </div>
        )
      }
    })

export default PizzaMaps;