import React from 'react';
import './userLocation.css';
import PizzaMaps from '.././PizzaMaps/PizzaMaps';
console.log('usr loc')
var userLocation = React.createClass({
    getInitialState: function() {
      return {
      userLat: null,
        userLong: null
      }
    },

    componentDidMount() {
      console.log('User Mount')
      this.usersLocation()
    },

    updateUserLocation: function(event)  {
      console.log('Print Event Coords: ', event.coords);
      console.log('Print Timestamp: ', event.timestamp);
        this.setState({
          userLat: event.coords.latitude,
          userLong: event.coords.longitude,
        });
      return event.coords;
    },
    usersLocation: function() {
      if (navigator.geolocation) {
        console.log('User Allowed geolocation');
          return navigator.geolocation.getCurrentPosition(this.updateUserLocation);
      } else {
        // Add error/denial handling here
        console.error('error/denial ');
      }
    },
    render() {
      return (
        <div>
          <h1>Google Maps</h1>
          <h2>Users Location</h2>
            <ul>
              <li>Latitude {this.state.userLat}</li>
              <li>Longitude {this.state.userLong}</li>
            </ul>
            <PizzaMaps name="PizzaMap Comp" userLat={this.state.userLat} userLong={this.state.userLong}/>
        </div>
      )
    }
  })

export default userLocation;