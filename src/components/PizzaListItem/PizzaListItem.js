import React from 'react';
import axios from 'axios';
import './PizzaListItem.css';
import Paper from 'material-ui/Paper';
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import userLocation from '../userLocation/userLocation';
let localDB = 'userFavs1';

function checkLocalStorage(cords){
  let localArr = localStorage.getItem(localDB) !== null ?  localStorage.getItem(localDB).split(',') : []
  if (localArr.indexOf(cords.props.data.geometry.coordinates.join('')) !== -1) {
    return true;
  } else {
    return false
  }
}
function updateLocal(add, cords){
  let localArr = localStorage.getItem(localDB) !== null ?  localStorage.getItem(localDB).split(',') : []
  if (add) {
    localArr.push(cords);
    localStorage.setItem(localDB,localArr)
  } else {
    let index = localArr.indexOf(cords);
    if (index > -1) {
      localArr.splice(index, 1)
    }
  }
  localStorage.setItem(localDB, localArr)
}
function toggleState(){
  updateLocal(!this.state.isFav, this.props.data.geometry.coordinates.join(''))
  this.setState({
    isFav: !this.state.isFav
  })
}


class PizzaItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFav: false,
      zDepth: 1
    };
  }

 static childContextTypes =
    {
        muiTheme: React.PropTypes.object
    }

    getChildContext()
    {
        return {
            muiTheme: getMuiTheme()
        }
    }
  componentDidMount() {
    if (checkLocalStorage(this)) {
      this.setState({
        isFav: true, 
      })
    }
  }
  mouseEnter(){
    this.setState({
        zDepth: 5, 
      })
  }
    mouseLeave(){
    this.setState({
        zDepth: 1, 
      })
  }
  render() {
          let favClass;
          if (this.state.isFav === true) {
            favClass = "is-fav user-fav"
          } else {
            favClass = "is-fav"
          }
    return (
      <Paper zDepth={this.state.zDepth} className='paper' onMouseEnter={this.mouseEnter.bind(this)} onMouseLeave={this.mouseLeave.bind(this)}>
        <div key={this.props.data.geometry.coordinates.join('')} className="parlor" data-cords={this.props.data.geometry.coordinates} >
          <div  className={favClass} onClick={toggleState.bind(this)}></div>
          <h2>{this.props.data.properties.pizzeria}</h2>
          <a href={this.props.data.properties.website} target="_Blank">{this.props.data.properties.website}</a>
          
          <p> {this.props.data.properties.address} {this.props.data.properties.city}</p>
        </div>
      </Paper>
    );
  }
}

  var PizzaListItem = React.createClass({
    getInitialState: function() {
      return {
        pizzaParlors: [], 
        userLoc: {userLocation}
      }
    },
    componentDidMount: function() {
      let that = this;
      this.serverRequest = axios.get(this.props.source)
          .then(function(result) {
            that.setState({
              pizzaParlors: result.data
            });
          })
    },
    componentWillUnmount: function() {
      // this.serverRequest.abort();
    },
    render: function() {
      return (
        <div className="flex-container">

          {
            this.state.pizzaParlors.map(function(parlor, index) {
            return (
                  <PizzaItem key={parlor.geometry.coordinates} data={parlor} />
            );
          })
        }
        </div>
      )
    }
  })

export default PizzaListItem;   