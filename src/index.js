import React from 'react';
import ReactDOM from 'react-dom';
import { Route,  HashRouter } from 'react-router-dom';
import App from './App';
import Nav from './components/Nav/Nav';
import Header from './components/Header/Header';

import PizzaParlors from './components/PizzaParlors/PizzaParlors';
import userLocation from './components/userLocation/userLocation';



import './index.css';

var destination = document.getElementById('root');
var Routes = [
  {
    name: 'Home',
    path: "/",
  },
  {
    name: 'Pizza Parlors',
    path :"/pizza"
  },
  {
    name: 'Maps',
    path :"/maps"
  }
]
ReactDOM.render(
  <HashRouter>
      <div>
      <Header />
        <Nav routes={Routes}/>

        <Route exact path="/" component={App} />
        <Route exact path="/pizza" component={PizzaParlors} />
        <Route exact path="/maps" component={userLocation} />
      </div>
    </HashRouter>,
  destination
);